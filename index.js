const http = require('http');
const Bundle = require('bono');
const serve = require('koa-static');

const authMiddleware = require('./middlewares/auth');
const { HTTP_PORT, ONE_HOUR } = require('./config');
// const manager = require('./middlewares/database');

const api = require('./api');
const auth = require('./auth');
const asset = require('./assets');

const app = new Bundle();

app.use(require('bono/middlewares/json')());
asset.use(serve('./uploads'));

api.use(authMiddleware);

app.bundle('/api', api);
app.bundle('/auth', auth);
app.bundle('/assets', asset);

app.get('/', (ctx) => 'hello world');

const server = http.createServer(app.callback());

server.listen(HTTP_PORT, '0.0.0.0');
// server.listen(HTTP_PORT, () => console.log(`server listen on port ${HTTP_PORT}`));

// function doSome () {
//   manager.runSession(async session => {
//     let data = await session.factory('Games').single();
//     console.log(data);
//   });
//   setTimeout(doSome, ONE_HOUR * 2)
// }

// doSome();
