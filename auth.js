const Bundle = require('bono');
const jwt = require('jsonwebtoken');
const manager = require('./middlewares/database');
const crypto = require('crypto');

const { secretJwt } = require('./config');

class Auth extends Bundle {
  constructor () {
    super();
    this.post('/login', this.login.bind(this));
    this.post('/register', this.register.bind(this));
  }

  async login (ctx) {
    let { username, password } = await ctx.parse();
    return manager.runSession(async (session) => {
      let passHash = crypto.createHash('md5').update(password).digest('hex');
      let data = await session.factory('users').find({ username, password: passHash }).single();
      if (data) {
        let token = jwt.sign({ userData: data }, secretJwt, { expiresIn: 60 * 60 * 24 * 361 });
        return { error: false, token };
      }
      return { error: true, token: null, username: undefined };
    });
  }

  async register (ctx) {
    let body = await ctx.parse();

    try {
      await manager.runSession(async (session) => {
        let password = crypto.createHash('md5').update(body.password).digest('hex');
        Object.assign(body, {
          password,
        });
        await session.factory('users').insert(body).save();
      });
      return { error: false, msg: 'Success Register' };
    } catch (error) {
      return { error: true, msg: error.message };
    }
  }
}

module.exports = new Auth();
